__author__ = 'Robbie'

from unittest import TestCase
from buzzers_mysql import buzzers_mysql
from buzzers_mysql.buzzers_mysql import BuzzersMySql
from buzzer_dto.user import User


class TestBuzzersMySQL(TestCase):

    def setUp(self):
        pass

    def test_create_user_that_does_not_exits(self):
        mysql = BuzzersMySql()
        username = "robbie"
        password = "robbie"
        usertype = "robbie"
        self.assertTrue(mysql.create_user(username, password, usertype))

    def test_create_user_that_already_exists(self):
        mysql = BuzzersMySql()
        username = "admin"
        password = "admin"
        usertype = "admin"
        activated = 1
        self.assertFalse(mysql.create_user(username, password, usertype, activated))

    def test_get_admin_user(self):
        mysql = BuzzersMySql()
        username = "admin"
        password = "admin"
        user = mysql.get_user(username, password)
        self.assertEqual(username, user.username)
        self.assertEqual(password, user.password)
        self.assertEqual('admin', user.usertype)
        self.assertEqual(1, user.activated)

    def test_get_unactivated_users(self):
        mysql = BuzzersMySql()
        list_of_users = mysql.get_unactivated_users()

    def test_activate_user(self):
        mysql = BuzzersMySql()
        username = 'rob'
        self.assertTrue(mysql.activate_user(username))

    def test_get_flights_as_dictionary(self):
        mysql = BuzzersMySql()
        flight_dict = mysql.get_flights_as_dictionary()
        print("SDf")

    def test_create_database(self):
        buzzers_mysql.create_database_and_tables()
        print("mm")

    def test_get_user_with_type(self):
        mysql = BuzzersMySql()
        pilot_dict = mysql.get_users_of_type_as_json("pilot")
        print("pause")