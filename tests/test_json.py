__author__ = 'Robbie'

from unittest import TestCase
from buzzdata import buzzer, data_for
import json
from buzzer_jsonifier import Jsonifier


class TestJson(TestCase):

    def setUp(self):
        pass

    def test_datafor_to_json(self):
        #pilot
        pilot_data = data_for(buzzer.pilot)
        print(json.dumps(dict(pilot_data)))

        #team
        team_data = data_for(buzzer.team)
        print(json.dumps(dict(team_data)))

        #crew
        crew_data = data_for(buzzer.pilot)
        print(json.dumps(dict(crew_data)))

    def test_ordered_tuple_to_json(self):
        print(Jsonifier.tuple_to_json(data_for(buzzer.crew)))
