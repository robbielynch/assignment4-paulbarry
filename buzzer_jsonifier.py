__author__ = 'Robbie'
import json

class Jsonifier(object):

    def __init__(self):
        pass

    @staticmethod
    def tuple_to_json(t):
        """
        Instead of converting it all to a dict, then to json, I've
        converted each element seperately to maintain the sorted order
        of each element. Because when converting to dict, the ordering
        was lost :(
        """
        jsonstring = "{ "
        for i in t:
            jsonstring = jsonstring + json.dumps(i[0]) + ":" + json.dumps(i[1]) + ","
        # Remove the last comma
        jsonstring = jsonstring[:len(jsonstring) - 1]
        return jsonstring + " }"
