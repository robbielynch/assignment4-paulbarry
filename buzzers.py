"""
Student Name    :   Robbie Lynch
Student Number  :   C00151101
Program         :   0. Updates to include api for activated users
                        - Json encoding happens in the buzzerz_mysql file
                        - In the get_users_of_type_as_json method
                        - Used single JS function to handle ajax requests in /static/js/buzzers.js
                    ===============================================================================
                    1. Working using the MySQL database
                    2. All database operations are located in the class buzzers_mysql/BuzzersMySql
                    3. In the buzzdata file, the flights are imported from mysql as a dictionary
                    4. Super Mario used this app and died. :(
Instructions    :   Make your database username and password == 'root'
"""

from functools import wraps
from flask import Flask, render_template, request, redirect, url_for, session
from buzzdata import buzzer, data_for
from buzzers_mysql.buzzers_mysql import BuzzersMySql
from buzzers_mysql import buzzers_mysql
from buzzer_dto.user import User
from buzzer_jsonifier import Jsonifier


webapp = Flask(__name__)

def check_logged_in(func):
    """ This decorator function checks to see that a valid user is 
        logged in. If yes, run the called function. If not, send
        the user to the login page. 
    """
    @wraps(func)
    def wrapped_function(*args, **kwargs):
        if 'logged-in' in session:
            return(func(*args, **kwargs))
        else:
            return render_template( 'nologin.html', 
                                    title='Authorization Needed',
                                    nologin=url_for("dologin") )
    return wrapped_function

def checkOKtype(utype):
    """ This (parameterised) decorator makes sure the user can only look at the 
        content that's meant for them (unless they are of type 'admin', in which 
        case they can see EVERYTHING).
    """
    def check_usertype(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            if session['usertype'] in (utype, 'admin'):
                return(func(*args, **kwargs))
            else:
                return redirect(url_for('indexpage'))
        return wrapped_function
    return check_usertype

@webapp.route('/')
@check_logged_in
def indexpage():
    """ The home page for this webapp. Displays a menu of options
        for the user to choose from.
    """

    ## Another strategy here could be to only display the menu options that are applicable 
    ## to the specific user-type logged in. This may result in more logic in the code, or more
    ## logic in the templates, which may or may not be advisable/worthwhile. From a UI prespective,
    ## it may be better to limit the users options, reducing their frustration at "non-working" 
    ## options.

    return render_template("index.html",
                           title="Bahamas Buzzers",
                           logged_in_as=session['userid'],
                           usertype=session['usertype'],
                           show_logout=True)

@webapp.route('/login', methods=["GET", "POST"])
def dologin():
    #TODO login. matching against mysql datastore
    if request.method == "GET":
        return render_template("login.html",
                               title='Please login')
    elif request.method == "POST":
        mysql = BuzzersMySql()
        u = request.form['userid']
        p = request.form['passwd']

        user = mysql.get_user(u, p)
        if user:
            if user.activated == 1:
                session['logged-in'] = True
                session['userid'] = u
                session['passwd'] = p
                session['usertype'] = user.usertype
                return render_template("index.html",
                                       title="Bahamas Buzzers",
                                       logged_in_as=session['userid'],
                                       usertype=session['usertype'],
                                       show_logout=True)

    return redirect(url_for('dologin'))

@webapp.route('/register', methods=["GET", "POST"])
def doregistration():
    #TODO - Register using mysql
    if request.method == "GET":
        return render_template("register.html",
                               title="Registration")
    elif request.method == "POST":
        mysql = BuzzersMySql()
        u = request.form['userid']
        p = request.form['passwd']
        t = request.form['usertype']

        user_created = mysql.create_user(u, p, t)
        if user_created:
            return redirect(url_for('dologin'))
        else:
            return redirect(url_for('doregistration'))
    else:
        return redirect(url_for('doregistration'))

@webapp.route('/pilot')
@check_logged_in
@checkOKtype('pilot')
def pilot():
    """ Render the pilot's data.
    """
    return render_template("display_data.html",
                           title="Data for Pilots",
                           columns=('Destination', 'Times',),
                           data=data_for(buzzer.pilot),
                           show_logout=True)

@webapp.route('/team')
@check_logged_in
@checkOKtype('team')
def team():
    """ Render the departures team's data.
    """
    return render_template("display_data.html",
                           title="Data for Departures Team",
                           columns=('Time', 'Destination',),
                           data=data_for(buzzer.team),
                           show_logout=True)

@webapp.route('/crew')
@check_logged_in
@checkOKtype('crew')
def crew():
    """ Render the booking crew's data.
    """
    return render_template("display_data.html",
                           title="Data for Booking Crew",
                           columns=('Destination', 'Time',),
                           data=data_for(buzzer.crew),
                           show_logout=True)

@webapp.route('/admin', methods=["GET", "POST"])
@check_logged_in
@checkOKtype('admin')
def switch_on_users():
    """ The administrator's functionality. Display the list of users waiting
        to be enabled, then let the admin select who to switch on, then (on
        submit) enable the users identified.
    """
    #TODO
    if request.method == "GET":
        # Grab the data from _WAITING and create a webpage which allows the 
        # administrator to enable users.
        mysql = BuzzersMySql()
        waiting_users = mysql.get_unactivated_users()
        if len(waiting_users) < 1:
            waiting_users = "There are no users waiting to be activated"

        return render_template("admin.html",
                               title = "Admin: Enable New Users",
                               data=waiting_users,
                               show_logout=True)
    elif request.method == "POST":
        mysql = BuzzersMySql()
        for user in request.form:
            mysql.activate_user(user)
        return redirect(url_for("indexpage"))
    else:
        return render_template("index.html",
                               title="Bahamas Buzzers",
                               logged_in_as=session['userid'],
                               usertype=session['usertype'],
                               show_logout=True)

@webapp.route('/logout')
@check_logged_in
def dologout():
    """ Logout a logged-in user, being sure to remove the current user's data 
        from the session dictionary.
    """
    session.pop('logged-in', None)
    session.pop('userid', None)
    session.pop('passwd', None)
    session.pop('usertype', None)
    return render_template("login.html",
                           title="You are now logged out.")

@webapp.route('/apitester')
@check_logged_in
def apitester():
    return render_template("api_tester.html",
                           title="Bahamas Buzzers Awesome API Testing Facility",
                           logged_in_as=session['userid'],
                           usertype=session['usertype'],
                           show_logout=True)

@webapp.route('/api/pilot')
def api_pilot():
    return Jsonifier.tuple_to_json(data_for(buzzer.pilot))

@webapp.route('/api/crew')
def api_crew():
    return Jsonifier.tuple_to_json(data_for(buzzer.crew))

@webapp.route('/api/team')
def api_team():
    return Jsonifier.tuple_to_json(data_for(buzzer.team))



##CREATE DATABASE, TABLES, AND INSERT DATA
buzzers_mysql.create_database_and_tables()

if __name__ == '__main__':
    webapp.secret_key = b'youwillneverguessmysecretkeyhahahahahahaaaaaaaaaaa'
    webapp.run(debug=True, host='0.0.0.0')
