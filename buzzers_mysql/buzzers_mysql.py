__author__ = 'Robbie'
import mysql.connector
from buzzer_dto.user import User
import json

######################################
#Variables for database connection
######################################
USER='root'
PASSWORD='root' #If you don't use a password, then you'll have to comment out the password variables in the code below
HOST='127.0.0.1'
DB_NAME='robbiebuzzers'
######################################
#End variables for database connection
######################################

class BuzzersMySql(object):

    connection = ""
    cursor = ""

    def __init__(self):
        """
        Opens a connection to the database.
        Initialises the class variables:
            - connection    The connection to the database
            - cursor        The cursor to the database
        """
        try:
            connection = mysql.connector.connect(host=HOST,
                                                 user=USER,
                                                 password=PASSWORD, #Comment out this line if you don't use a password
                                                 database=DB_NAME, )
            self.connection = connection
            self.cursor = connection.cursor()
        except mysql.connector.Error as err:
            print("Failed to connect to database: {}".format(err))
            exit(1)

    def create_user(self, username, password, usertype, activated=0):
        """
        Creates a user if it does not exist.
        If it already exist, it returns false.
        """
        if not self.get_user(username, password):
            SQL = """INSERT INTO user
                    (username,password,usertype,activated)
                    VALUES (%s,%s,%s,%s);
                    """
            try:
                self.cursor.execute(SQL, (username, password, usertype, activated))
                self.connection.commit()
                return True
            except Exception:
                self.connection.rollback()
                return False
        return False

    def get_user(self, username, password):
        """
        Method to search the database for a user.
        @param  username - string
        @param  password - string
        @return user     - if found, object representation of the user
        @return None     - if not found
        """
        try:
            SQL = """SELECT * FROM user WHERE username=%s and password=%s"""
            self.cursor.execute(SQL, (username,password,))
            id, u, p, t, a = self.cursor.fetchone()
            user_obj = User(u, p, t, a, id)
            return user_obj
        except:
            return None

    def get_unactivated_users(self):
        """
        Returns a python list of unactivated user objects
        """
        list_of_users = []
        try:
            SQL = """SELECT * FROM user WHERE activated=0"""
            self.cursor.execute(SQL)
            for user in self.cursor.fetchall():
                user_obj = User(user[1], user[2], user[3], user[4], user[0])
                if user_obj:
                    list_of_users.append(user_obj)
            return list_of_users
        except:
            return list_of_users

    def activate_user(self, username):
        """
        Activates the given username
        """
        try:
            SQL = """UPDATE user SET activated=1 WHERE username=%s"""
            try:
                self.cursor.execute(SQL, (username,))
                self.connection.commit()
                return True
            except:
                self.connection.rollback()
                return False
        except:
            return False

    def get_flights_as_dictionary(self):
        """
        Returns all flight information as a python dictionary
        """
        SQL = "SELECT * FROM flight"
        flight_dict = {}
        try:
            self.cursor.execute(SQL)
            for flight in self.cursor.fetchall():
                flight_dict[flight[1]] = flight[2]
            return flight_dict
        except:
            return flight_dict

    def get_users_of_type_as_json(self, usertype):
        """
        Finds all users of a given type. Convert them into
        JSON format and returns them.
        """
        #Every json response will have a status of "ok" or "error".
        #If the user list is returned, it will definitly have
        #a status of "ok"
        user_list = ['{"status": "ok"}']
        try:
            SQL = """SELECT * FROM user WHERE usertype=%s AND activated=1"""
            self.cursor.execute(SQL, (usertype,))
            for user in self.cursor.fetchall():
                #Convert each user tuple reply into a user object
                user_obj = User(user[1], user[2], user[3], user[4], user[0])
                #Serialise user object and add to list of users
                user_list.append(json.dumps(user_obj.__dict__))
            #Concatenate json strings into one string
            json_return_string = '[ ' + ",".join(user_list) + " ]"
            return json_return_string
        except Exception as e:
            return '[ {"status": "error", "message": "Error occurred while trying to retrieve users" } ]'


def create_flights(cursor, cnx):
   """
   Creates all flights in the flight table.
   """
   SQLs = ["INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('09:35', 'Freeport')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('17:00', 'Freeport')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('19:00', 'Freeport')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('09:55', 'West End')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('10:45', 'Treasure Cay')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('11:45', 'Rock Sound')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('17:55', 'Rock Sound')",
           "INSERT INTO " + DB_NAME + ".flight (time,destination) VALUES ('12:00', 'Arthurs Town')"
   ]
   cursor.execute("Select count(*) from {}.flight".format(DB_NAME))
   (number_of_rows,)=cursor.fetchone()
   if number_of_rows < 1:
       for sql in SQLs:
           try:
               cursor.execute(sql)
               cnx.commit()
           except Exception:
               return False
   return True


def create_database_and_tables():
        """
        Creates the original database and tables using the database variables of the class.
        """
        cnx = mysql.connector.connect(user=USER
                                        ,password=PASSWORD  #Comment out this line if you don't use a password
                                    )
        cursor = cnx.cursor()
        try:
            #Create Database
            cursor.execute(
                "CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
            #Create tables
            cursor.execute(CREATE_USER_TABLE)
            cnx.commit()
            cursor.execute(CREATE_FLIGHT_TABLE)
            cnx.commit()
            #Create admin user
            try:
                create_admin_sql = """INSERT INTO """ + DB_NAME + """.user
                        (username,password,usertype,activated)
                        VALUES ('admin','admin','admin',1);"""
                cursor.execute(create_admin_sql)
                cnx.commit()
            except:
                pass
            #Create flights data in database
            try:
                create_flights(cursor,cnx)
            except:
                pass

        except mysql.connector.Error as err:
            print("Failed creating database and tables: {}".format(err))
            exit(1)


CREATE_USER_TABLE="""
    CREATE TABLE IF NOT EXISTS `""" + DB_NAME + """`.`user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(45) NOT NULL,
    `password` VARCHAR(45) NOT NULL,
    `usertype` VARCHAR(45) NOT NULL,
    `activated` TINYINT NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    UNIQUE INDEX `username_UNIQUE` (`username` ASC));
    """

CREATE_FLIGHT_TABLE="""
    CREATE TABLE IF NOT EXISTS `""" + DB_NAME + """`.`flight` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `time` VARCHAR(45) NOT NULL,
    `destination` VARCHAR(45) NOT NULL,
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    PRIMARY KEY (`id`));
    """