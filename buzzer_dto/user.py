__author__ = 'Robbie'

class User(object):
    id = ""
    username = ""
    password = ""
    usertype = ""
    activated = ""

    def __init__(self, username, password, usertype, activated=0, id=0):
        self.username = username
        self.usertype = usertype
        self.password = password
        self.activated = activated
        self.id = id
