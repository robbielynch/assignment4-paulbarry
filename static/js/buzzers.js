function get_users(usertype){
    var xmlhttp;
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById("mytextarea").innerHTML=xmlhttp.responseText;
        }
    }
    if(usertype === "pilot"){
        xmlhttp.open("GET","/api/pilot",true);
        xmlhttp.send();
    }
    else if(usertype === "team"){
        xmlhttp.open("GET","/api/team",true);
        xmlhttp.send();
    }
    else if(usertype === "crew"){
        xmlhttp.open("GET","/api/crew",true);
        xmlhttp.send();
    }
}

